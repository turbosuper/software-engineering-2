#ifndef ORDER_IF_H
#define ORDER_IF_H

#include <string>
#include <vector>
#include "../classes/pseudo_classes/ingredientamount.h"
using namespace std;

class order_if {
public:
    virtual int doOrder(vector<ingredientAmount>& products) = 0; //Return BestellID
    virtual int getLastID() = 0;
    virtual vector<int> getAllOrderIDs() = 0;
    //virtual set<int> iDs; - not in interface
};

#endif // ORDER_IF_H
