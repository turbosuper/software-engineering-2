#ifndef RECIPEDATABASE_H
#define RECIPEDATABASE_H

#include "recipe.h"
#include "../../interfaces/recipe_if.h"
#include <vector>
#include <string>

class recipedatabase :public recipe_if{
private:
    std::vector<recipe*> recipes;
public:
    recipedatabase();
    int addRecipe(std::string name, std::string instructions, std::vector<ingredient*> &ingredients);
    int delRecipe(std::string name);
    recipe* getRecipe(std::string name);
    void getAllRecipeNames(std::vector<std::string> &TargetVect);
};

#endif // RECIPEDATABASE_H
