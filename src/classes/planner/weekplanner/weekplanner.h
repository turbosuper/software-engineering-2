// Created by: Pierre Meißner
// Signleton Class Weekplanner
// Manages planned days for a week - planned dishes for a day must seperate made before on a new weekday instance.
// Can be used by its interface wpif and instanced with getInstance call with the initial Date of type time_t

#ifndef WEEKPLANNER_H
#define WEEKPLANNER_H
#include <ctime>
#include <vector>
#include "../../../interfaces/wp_if.h"
#include "../../../interfaces/order_if.h"
#include "../weekday/weekday.h"

class weekplanner : public wpif
{
private:
    std::vector<weekday> days;
    time_t currentDate;

    weekplanner(time_t initialDate);
    weekplanner(const weekplanner& orig) = delete;
    const weekplanner &operator = (const weekplanner& orig) = delete;
public:
    static weekplanner& getInstance(time_t initialDate);
    virtual std::vector<weekday> getNextDays(int count);
    virtual weekday* getWeekDay(time_t Day);
    virtual void SaveWeekday(weekday& wd);
    virtual bool DeleteWeekday(time_t Day);
    virtual void generateOrderForNexDay(order_if& OrderIface);
    virtual void UpdateDate(time_t now);
};

#endif // WEEKPLANNER_H
