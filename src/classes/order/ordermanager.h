#ifndef ORDERMANAGER_H
#define ORDERMANAGER_H
#include <vector>
#include "../../interfaces/order_if.h"
#include "../pseudo_classes/ingredientamount.h"

class ordermanager : public order_if
{
private:
    vector<int> iDs;
public:
    ordermanager();
    virtual int getLastID();
    virtual vector<int> getAllOrderIDs();
    virtual int doOrder(vector<ingredientAmount>& products); //Return BestellID
};

#endif // ORDERMANAGER_H
