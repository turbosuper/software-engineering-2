#include "recipedatabase.h"

recipedatabase::recipedatabase(){}
int recipedatabase::addRecipe(std::string name, std::string instructions, std::vector<ingredient*> &ingredients){
    recipe& newRecipe = *new recipe(name, instructions, ingredients);
    recipes.push_back(&newRecipe);
}
int recipedatabase::delRecipe(std::string name){
    std::vector<recipe*>::iterator it;
    for (it = recipes.begin(); it != recipes.end(); ++it) {
        recipe& element = (**it);
        if(element.getName() == name){ // found
            recipes.erase(it);
        }
    }
}
recipe* recipedatabase::getRecipe(std::string name){
    for(auto element : recipes){
        if(element->getName() == name){
            return element;
        }
    }
    return nullptr;
}
void recipedatabase::getAllRecipeNames(std::vector<std::string> &TargetVect){
    for(auto element : recipes){
        TargetVect.push_back(element->getName());
    }
}
