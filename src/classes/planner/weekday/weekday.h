#ifndef WEEKDAY_H
#define WEEKDAY_H
#include <ctime>
#include <vector>
#include <string>
#include "../../pseudo_classes/recipevolume.h"
#include "../../recipe/recipe.h"
#include "../../recipe/ingredient.h"
#include "../../../interfaces/order_if.h"
#include "../../../interfaces/recipe_if.h"
#include "../../pseudo_classes/ingredientamount.h"

    class weekday{
    private:
        time_t date;
        std::vector<recipevolume> dishes;
        bool doneOrder;
        int OrderOfDay;
        bool isIngridientInVector(std::vector<ingredientAmount>& vect, ingredient& ingr);
        void AddIngAmountToIngidient(std::vector<ingredientAmount>& vect, std::string key, int Adder);

    public:
        weekday(std::string RecipeID, unsigned int Amount, std::string ForPlanned,time_t Date, recipe_if& RecipeIF);
        ~weekday();

        recipe* getPlannedRecipe(std::string planned);
        recipevolume* getCompleteRecipevolume(std::string planned);
        unsigned int getVolumeAmount(std::string planned);
        std::vector<std::string> getPlanningStrings();
        int countPlannedRecips();
        time_t getDate();
        int getDoneOrderID();

        bool insertRecipevolume(std::string RecipeID, unsigned int Amount, std::string ForPlanned, recipe_if& RecipeIF);
        void setDate(time_t Date);
        bool DoOrder(order_if& orderman);
    };

#endif // WEEKDAY_H
