#ifndef INGREDIENT_H
#define INGREDIENT_H

#include <string>

enum volumeType{
     Kg, //that means with '0' we point on Kg
     Litre, //'1'
     Item //'0'
};

class ingredient{
private:
    std::string name;
    float quantity;
    int articleNum;
    float price;
    volumeType volumeUnit;

public:
    ingredient(std::string name, volumeType unit, float quantity, int articleNum, float Price);
    std::string getName();
    int getQuantity();
    int getArticleNum();
    int getVolumeUnit();
    float getPrice();

};

#endif // INGREDIENT_H
