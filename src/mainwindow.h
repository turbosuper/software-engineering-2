#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QListWidget>
#include <QListWidgetItem>
#include <iostream>
#include <QDate>
#include <ctime>
#include "interfaces/wp_if.h"
#include "interfaces/recipe_if.h"
#include "classes/planner/weekplanner/weekplanner.h"
#include "classes/planner/weekday/weekday.h"
#include "classes/recipe/recipedatabase.h"
#include "classes/order/ordermanager.h"
#include "classes/recipe/recipe.h"
#include "classes/recipe/ingredient.h"
#include "recipeview.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_calendarWidget_clicked(const QDate &date);

    void on_ScheduedEventList_itemClicked(QListWidgetItem *item);

    void on_cmdRecipeToViewer_clicked();

    void on_cmdDeleteWD_clicked(QDate &date =0);

    void on_cmdShowBill_clicked();

    void on_pushButton_2_clicked();

signals:
    void dateChanged(QDate date);

private:
    Ui::MainWindow *ui;
    recipe_if& recipeDB = *new recipedatabase;
    order_if& ordermodule = *new ordermanager;
    wpif& planner = weekplanner::getInstance(time(nullptr)); // Singleton reference
    weekday* currentWD = nullptr;
    time_t qdate2timeT(const QDate& Date);
    std::string eventKey = "";
    recipeView *recipeViewWindow;
};

#endif // MAINWINDOW_H
