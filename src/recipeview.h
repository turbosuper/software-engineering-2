#ifndef RECIPEVIEW_H
#define RECIPEVIEW_H

#include <QDialog>

namespace Ui {
class recipeView;
}

class recipeView : public QDialog
{
    Q_OBJECT

public:
    explicit recipeView(QWidget *parent = 0, QString recipeName = 0, QString recipeSteps = 0);
    ~recipeView();

private slots:
    void on_listView_activated(const QModelIndex &index);

private:
    Ui::recipeView *ui;
};

#endif // RECIPEVIEW_H
