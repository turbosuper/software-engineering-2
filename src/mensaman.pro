#-------------------------------------------------
#
# Project created by QtCreator 2019-05-17T10:59:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mensaman
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    classes/recipe/ingredient.cpp \
    classes/recipe/recipe.cpp \
    classes/recipe/recipedatabase.cpp \
        main.cpp \
        mainwindow.cpp \
    classes/planner/weekday/weekday.cpp \
    classes/planner/weekplanner/weekplanner.cpp \
    classes/order/bill.cpp \
    classes/order/order.cpp \
    classes/order/ordermanager.cpp \
    recipeview.cpp

HEADERS += \
    classes/recipe/recipedatabase.h \
        mainwindow.h \
    interfaces/gui_interface.h \
    interfaces/order_if.h \
    interfaces/recipe_if.h \
    interfaces/wp_if.h \
    classes/recipe/recipe.h \
    classes/planner/weekday/weekday.h \
    classes/order/order.h \
    classes/pseudo_classes/recipevolume.h \
    classes/planner/weekplanner/weekplanner.h \
    classes/recipe/ingredient.h \
    classes/order/bill.h \
    classes/order/order.h \
    classes/order/ordermanager.h \
    classes/pseudo_classes/ingredientamount.h \
    recipeview.h

FORMS += \
        mainwindow.ui \
    recipeview.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
