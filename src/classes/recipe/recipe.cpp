#include "recipe.h"

recipe::recipe(std::string name, std::string instructions, std::vector<ingredient*> &ingredients){
    this->name = name;
    this->instructions = instructions;
    this->ingredients = ingredients;
}
std::string recipe::getName(){return name;}
std::string recipe::getInstructions(){return instructions;}
void recipe::getIngredients(std::vector<ingredient *> &TargetVect){
    std::vector<ingredient*>::iterator it;
    for (it = ingredients.begin(); it != ingredients.end(); ++it) { // Need to copy element by iterate into TargetVect for return
        TargetVect.push_back(*it);
    }
}
