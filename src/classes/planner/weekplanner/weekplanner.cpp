#include "weekplanner.h"

weekplanner::weekplanner(time_t initialDate) : wpif(){
    this->currentDate = initialDate;
}

weekplanner& weekplanner::getInstance(time_t initialDate){
    static weekplanner _instance(initialDate);
    return _instance;
}

bool weekplanner::DeleteWeekday(time_t Day){
    std::vector<weekday>::iterator it;
    for (it = days.begin();it != days.end();++it) {
        if (it->getDate() == Day){
            weekday& wd = *it; // wd never used: if func stack gets destructed, wd's destructor getting called autom.
            days.erase(it);
            return true;
        }
    }
    return false;
}

weekday* weekplanner::getWeekDay(time_t Day){
    std::vector<weekday>::iterator it;
    for (it = days.begin();it != days.end();++it) {
        if (it->getDate() == Day){
            return &(*it);
        }
    }
    return nullptr;
}

std::vector<weekday> weekplanner::getNextDays(int count){
    std::vector<weekday>::iterator it;
    std::vector<weekday> retvect;
    int iteration = 0;
    for (it = days.begin(); (it!= days.end()) && (iteration != count); ++it, ++iteration) {
        retvect.push_back(*it);
    }
    return retvect;
}

void weekplanner::UpdateDate(time_t now){
    currentDate = now;
}

void weekplanner:: generateOrderForNexDay(order_if& OrderIface){
    std::vector<weekday>::iterator it;
    for (it = days.begin();it != days.end();++it) {
        if(currentDate == it->getDate()){
            weekday& wd = *it;
            wd.DoOrder(OrderIface);
            break;
        }
    }
}

void weekplanner::SaveWeekday(weekday& wd){
    days.push_back(wd);
}
