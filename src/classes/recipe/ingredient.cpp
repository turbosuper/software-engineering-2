#include "ingredient.h"

ingredient::ingredient(std::string name, volumeType volumeUnit, float quantity, int articleNum, float price){
    this->name = name;
    this->volumeUnit = volumeUnit;
    this->quantity = quantity;
    this->articleNum = articleNum;
    this->price = price;
}
std::string ingredient::getName(){return name;}
int ingredient::getQuantity(){return quantity;}
int ingredient::getArticleNum(){return articleNum;}
int ingredient::getVolumeUnit(){return volumeUnit;}
float ingredient::getPrice(){return price;}
