#include "weekday.h"

weekday::weekday(std::string RecipeID, unsigned int Amount, std::string ForPlanned, time_t Date, recipe_if& RecipeIF){
    recipevolume& vol = *new recipevolume;
    vol.recipeptr = RecipeIF.getRecipe(RecipeID);
    vol.volume = Amount;
    vol.plannedFor = ForPlanned;
    dishes.push_back(vol);
    this->date = Date;
    this->OrderOfDay = 0;
    this->doneOrder = false;
}
weekday::~weekday(){
    std::vector<recipevolume>::iterator it; // prevent deleting recipe if only weekday gets destruction on its simple delete while the Application stays launched
    for (it = dishes.begin();it!=dishes.end();++it) {
        recipevolume* vol = &(*it);
        vol->recipeptr = nullptr;
    }
}

bool weekday::insertRecipevolume(std::string RecipeID, unsigned int Amount, std::string ForPlanned, recipe_if& RecipeIF){
    recipevolume& newvol = *new recipevolume;
    newvol.recipeptr = RecipeIF.getRecipe(RecipeID);
    newvol.volume = Amount;
    newvol.plannedFor = ForPlanned;
    dishes.push_back(newvol);
    return true;
}

void weekday::setDate(time_t Date){
    this->date = Date;
}

int weekday::countPlannedRecips(){
    return dishes.size();
}

time_t weekday::getDate(){
    return this->date;
}

int weekday::getDoneOrderID(){
    return this->OrderOfDay;
}

std::vector<std::string> weekday::getPlanningStrings(){
    std::vector<std::string> PlanningStrings;
    std::vector<recipevolume>::iterator it;
    for (it = dishes.begin(); it != dishes.end(); ++it) {
        PlanningStrings.push_back(it->plannedFor);
    }
    return PlanningStrings;
}

unsigned int weekday::getVolumeAmount(std::string planned){
    std::vector<recipevolume>::iterator it;
    for (it = dishes.begin(); it != dishes.end(); ++it) {
        if(it->plannedFor == planned){
            return it->volume;
        }
    }
    return 0;
}

recipevolume* weekday::getCompleteRecipevolume(std::string planned){
    std::vector<recipevolume>::iterator it;
    for (it = dishes.begin(); it != dishes.end(); ++it) {
        if(it->plannedFor == planned){
            return &(*it);
        }
    }
    return nullptr;
}

recipe* weekday::getPlannedRecipe(std::string planned){
    std::vector<recipevolume>::iterator it;
    for (it = dishes.begin(); it != dishes.end(); ++it) {
        if(it->plannedFor == planned){
            return it->recipeptr;
        }
    }
    return nullptr;
}

bool weekday::DoOrder(order_if& orderman){
    if (doneOrder) return true;
    std::vector<recipevolume>::iterator it;
    std::vector<ingredientAmount> iavect;
    for (it = dishes.begin(); it != dishes.end(); ++it) {
        std::vector<ingredient*>::iterator ingrIT;
        std::vector<ingredient*> ingridients;
        it->recipeptr->getIngredients(ingridients);
        for (ingrIT = ingridients.begin(); ingrIT != ingridients.end();++ingrIT) {
            ingredient& ingr = (**ingrIT);
            if (isIngridientInVector(iavect, ingr)){
                AddIngAmountToIngidient(iavect, ingr.getName(), ingr.getQuantity());
            } else {
                ingredientAmount* ins_ingr = new ingredientAmount;
                ins_ingr->IngridientID = ingr.getName();
                ins_ingr->Amount = ingr.getQuantity();
                iavect.push_back(*ins_ingr);
            }
        }
    }
    OrderOfDay = orderman.doOrder(iavect);
    if(OrderOfDay != 0){
        doneOrder = true;
        return true;
    }
    return false;
}

bool weekday::isIngridientInVector(std::vector<ingredientAmount>& vect, ingredient& ingr){
    std::vector<ingredientAmount>::iterator it;
    for (it = vect.begin(); it != vect.end(); ++it) {
        if (it->IngridientID == ingr.getName()){
            return true;
        }
    }
    return false;
}

void weekday::AddIngAmountToIngidient(std::vector<ingredientAmount>& vect, std::string key, int Adder){
    std::vector<ingredientAmount>::iterator it;
    for (it = vect.begin(); it != vect.end(); ++it) {
        if (it->IngridientID == key){
            it->Amount += Adder;
            return;
        }
    }
}
