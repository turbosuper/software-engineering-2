#ifndef BILL_H
#define BILL_H
#include <vector>
#include "../planner/weekday/weekday.h"
#include "../pseudo_classes/ingredientamount.h"

class Bill
{
private:
    float cost;
    std::vector<ingredientAmount> products;
public:
    Bill();
};

#endif // BILL_H
