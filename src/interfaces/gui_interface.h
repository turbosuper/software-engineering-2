#ifndef GUI_INTERFACE_H
#define GUI_INTERFACE_H
#include <string>

class GUI_Interface
{
public:
    virtual void showErrorMsg(std::string String); //Return BestellID
    virtual ~GUI_Interface();
};

#endif // GUI_INTERFACE_H
