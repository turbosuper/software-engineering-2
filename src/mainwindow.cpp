#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //ui->ScheduedEventList->addItem("STH");

}

MainWindow::~MainWindow()
{
    delete ui;
}

time_t MainWindow::qdate2timeT(const QDate &Date){
    struct tm timeconvert = {0};
    timeconvert.tm_year = Date.year();
    timeconvert.tm_mon = Date.month();
    timeconvert.tm_mday = Date.day();
    return mktime(&timeconvert);
}

void MainWindow::on_calendarWidget_clicked(const QDate &date)
{
    // convert QDAte to time_t
    const time_t searchTime = qdate2timeT(date);
    currentWD = planner.getWeekDay(searchTime);

    //send signal of chosen date
    if (date != NULL){
        emit dateChanged(date);
    }
    //show date on other forms
    QString lblPlannedForText = "Selected date: ";
    QString date_string = date.toString(Qt::TextDate);
    QString date_label = lblPlannedForText + date_string;
    ui->txtDateOutline->setText(date_label);

    ui->ScheduedEventList->clear();

    //show 'events' for the chosen weekday

    if (currentWD == nullptr){
        std::cerr << "No Weekday found!" << std::endl;
        //disable the recipe button
        ui->cmdRecipeToViewer->setEnabled(false);
        //clear the volume field
        ui->txtVolume->setText("");
        //disable weekday delete button
        ui->cmdDeleteWD->setEnabled(false);
    } else {
        std::vector<std::string>::iterator it;
        std::vector<std::string> planningStrings = currentWD->getPlanningStrings();
        for (auto element : planningStrings){
            ui->ScheduedEventList->addItem(QString::fromStdString(element));
         };

        // write data into ui elements
    }
}

void MainWindow::on_ScheduedEventList_itemClicked(QListWidgetItem *item)
{
    //show isOrdered
    QString currentEvent = item->text();
    eventKey = currentEvent.toStdString();

    if(currentWD->getDoneOrderID() == 0){
        ui->isOrdered->setChecked(false);
     }else{
        ui->isOrdered->setChecked(true);
    }
    //show Volume
    unsigned int volume = currentWD->getVolumeAmount(eventKey);
    ui->txtVolume->setText(QString::number(volume));

    //show recipe name
    recipe* currentRecipe = currentWD->getPlannedRecipe(eventKey);
    ui->lblRecipeName->setText(QString::fromStdString(currentRecipe->getName()));
    ui->cmdRecipeToViewer->setEnabled(true);
    ui->cmdDeleteWD->setEnabled(true);
}

void MainWindow::on_cmdRecipeToViewer_clicked()
{
 //get the recipe in question
 recipe* currentRecipe = currentWD->getPlannedRecipe(eventKey);

     //QString ingredients_list = QString::fromStdString(currentRecipe->getIngredients(currentRecipe->ingredients);
     QString recipe_name = QString::fromStdString(currentRecipe->getName());
     QString recipe_instruction = QString::fromStdString(currentRecipe->getInstructions());

     //create new window, and pass the strings to it
      recipeViewWindow = new recipeView(this, recipe_name, recipe_instruction);
      recipeViewWindow->show();
}

//QObject::connect(const QObject *, const char *signal, const char *member, Qt::ConnectionType type) const

void MainWindow::on_cmdDeleteWD_clicked(QDate &date)
{
    /*
    //get the date from calendar
    QDate date = ui->calendarWidget->data;
    const time_t calendarTime = qdate2timeT(date);

    //get the weekplanner for the date
    planner.DeleteWeekday(date)
*/
}

void MainWindow::on_cmdShowBill_clicked()
{

}

void MainWindow::on_pushButton_2_clicked() // cmd Plan new Weekday
{
    struct tm dummy_time = {0};
    dummy_time.tm_year = 2019;
    dummy_time.tm_mon = 7;
    dummy_time.tm_mday = 4;
    ingredient& milch = *new ingredient("Milch", volumeType::Litre, 0.5, 123, 0.75);
    ingredient& eier = *new ingredient("Eier", volumeType::Item, 2, 345, 0.55);
    std::vector<ingredient*> pancake_ingredients;
    pancake_ingredients.push_back(&milch);
    pancake_ingredients.push_back(&eier);

   // recipe& pancake = *new recipe("pancake", "all together and fry", pancake_ingredients);
    recipeDB.addRecipe("pancake", "all together and fry", pancake_ingredients);

    weekday& dummywd = *new weekday("pancake", 5, "Mittagessen1", mktime(&dummy_time), recipeDB);
   // dummywd.insertRecipevolume("kuchen", 12, "Mittagessen2", recipeDB);
   // dummywd.insertRecipevolume("cafe", 33, "Mittagessen3", recipeDB);
    planner.SaveWeekday(dummywd);
}
