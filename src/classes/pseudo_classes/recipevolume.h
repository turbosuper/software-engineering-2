#ifndef RECIPEVOLUME_H
#define RECIPEVOLUME_H
#include <string>
#include "../recipe/recipe.h"

/* Number of given recipes to prepare */
struct recipevolume{
    recipe* recipeptr;
    unsigned int volume;
    std::string plannedFor;
};

#endif // RECIPEVOLUME_H
