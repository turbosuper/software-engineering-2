#include "recipeview.h"
#include "ui_recipeview.h"

recipeView::recipeView(QWidget *parent, QString recipeName, QString recipeSteps) :
    QDialog(parent),
    ui(new Ui::recipeView)
{
    QString name_string = "Recipe:";
    QString steps_string = "Proceed with the following steps to prepare:";
    ui->setupUi(this);
    ui->listWidget->addItem(name_string);
    ui->listWidget->addItem(recipeName);
    ui->listWidget->addItem(steps_string);
    ui->listWidget->addItem(recipeSteps);

}

recipeView::~recipeView()
{
    delete ui;
}

void recipeView::on_listView_activated(const QModelIndex &index)
{

}
