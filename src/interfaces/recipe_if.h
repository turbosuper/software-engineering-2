#ifndef RECIPE_IF_H
#define RECIPE_IF_H

#include <list>
#include <string>
#include "../classes/recipe/recipe.h"

/*
 * Rezept:
 *  ID: 13
 *  Name: Nudeln
 *  Zutaten: Kartoffeln:200g:1345684646,Eier:2Stk:18767345,Mehl:200g:86547
 *  Beschreibung: Alle Zutaten zusammenschmeißen und bei 300°C für 30min köcheln lassen.
*/

class recipe_if{

public:
    virtual int addRecipe(std::string name, std::string instructions, std::vector<ingredient*> &ingredients) = 0;
    virtual int delRecipe(std::string name) = 0;
    virtual recipe* getRecipe(std::string name) = 0;
    virtual void getAllRecipeNames(std::vector<std::string> &TargetVect) = 0;
};

#endif // RECIPE_H
