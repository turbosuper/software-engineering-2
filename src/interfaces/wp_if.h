#ifndef WP_IF_H
#define WP_IF_H
// Created by Pierre Meissner
    #include <ctime>
    #include <vector>
    #include "../classes/planner/weekday/weekday.h"
    #include "../classes/recipe/recipe.h"
    #include "order_if.h"
    class wpif{
    public:
        virtual std::vector<weekday> getNextDays(int count) = 0;
        virtual weekday* getWeekDay(time_t Day) = 0;
        virtual void SaveWeekday(weekday& wd) = 0;
        virtual bool DeleteWeekday(time_t Day) = 0;
        virtual void generateOrderForNexDay(order_if& OrderIface) = 0;
        virtual void UpdateDate(time_t now) = 0;
    };

#endif // WP_IF_H
