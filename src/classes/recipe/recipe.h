#ifndef RECIPE_H
#define RECIPE_H

#include <string>
#include <vector>
#include "ingredient.h"

class recipe{
private:
	std::string name;
    std::string instructions;
    std::vector<ingredient*> ingredients;

public:
    
    recipe(std::string name, std::string instructions, std::vector<ingredient*> &ingredients);
    std::string getName();
    std::string getInstructions();
    void getIngredients(std::vector<ingredient*> &TargetVect);

};


#endif // RECIPE_H
